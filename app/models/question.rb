class Question < ApplicationRecord
  include PgSearch
  has_many :answers, :dependent => :destroy
  belongs_to :user
  default_scope { order(created_at: :DESC) }
  pg_search_scope :search, :against => [:title, :body]
end
